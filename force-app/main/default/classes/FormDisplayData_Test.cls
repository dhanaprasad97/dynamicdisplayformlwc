/*
 * Created by : Dhana Prasad
 * test class for FormDisplayData class
 */
@isTest
public  class FormDisplayData_Test {
    @isTest
    public static void formDisplayDataTest() {
        try{
            List<CIF_Form_Wizard_Configuration__mdt> cmd1 = FormDisplayData.getMetadata();
            System.assertEquals(cmd1.size()!=0,true);
        }Catch(Exception e){
            System.assertEquals(true, e.getMessage().contains('Script-thrown exception'));
        }       
    }
}
