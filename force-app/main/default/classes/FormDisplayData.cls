//Created by Dhana Prasad
//Class to fetch meta data for from creation
public with sharing class FormDisplayData {
    @AuraEnabled
    public static List<CIF_Form_Wizard_Configuration__mdt> getMetadata()
    {
        return [SELECT Id, Alignment__c, CIF_Form_Step_Name__c, Field_API_Name__c, Object_API_Name__c, Sequence__c,Single_Row__c,Type__c,MasterLabel
        FROM CIF_Form_Wizard_Configuration__mdt 
        WHERE CIF_Form_Step_Name__c = '1'
        ORDER BY Sequence__c,Alignment__c];
    }
}
