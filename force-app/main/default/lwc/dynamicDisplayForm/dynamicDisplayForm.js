import { LightningElement, track } from 'lwc';
import getMetadataOfStep1 from "@salesforce/apex/FormDisplayData.getMetadata";

export default class DynamicDisplayForm extends LightningElement {

    //track variables
    @track industryValue = 'Choose...';
    @track industryOptions = [
        { label: 'Choose...', value: 'Choose...' },
        { label: 'Advertising', value: 'Advertising' },
        { label: 'Aerospace & Defence', value: 'Aerospace & Defence' },
        { label: 'Agriculture', value: 'Agriculture' },
        { label: 'Apparel', value: 'Apparel' },
        { label: 'Banking', value: 'Banking' },
        { label: 'Biotechnology', value: 'Biotechnology' },
        { label: 'Business Services', value: 'Business Services' },
        { label: 'Chemicals', value: 'Chemicals' },
        { label: 'Communications', value: 'Communications' },
        { label: 'Construction', value: 'Construction' },
        { label: 'Consulting', value: 'Consulting' },
        { label: 'Education', value: 'Education' },
        { label: 'Electronics', value: 'Electronics' },
        { label: 'Energy', value: 'Energy' },
        { label: 'Engineering', value: 'Engineering' },
        { label: 'Entertainment', value: 'Entertainment' },
        { label: 'Environmental', value: 'Environmental' },
        { label: 'Finance', value: 'Finance' },
        { label: 'Food & Beverage', value: 'Food & Beverage' },
        { label: 'Government', value: 'Government' },
        { label: 'Healthcare', value: 'Healthcare' },
        { label: 'Hospitality', value: 'Hospitality' },
        { label: 'Insurance', value: 'Insurance' },
        { label: 'Legal', value: 'Legal' },
        { label: 'Machinery', value: 'Machinery' },
        { label: 'Manufacturing', value: 'Manufacturing' },
        { label: 'Media', value: 'Media' },
        { label: 'Not For Profit', value: 'Not For Profit' },
        { label: 'Other', value: 'Other' },
        { label: 'Recreation', value: 'Recreation' },
        { label: 'Retial', value: 'Retial' },
        { label: 'Shipping', value: 'Shipping' },
        { label: 'Technology', value: 'Technology' },
        { label: 'Telecommunications', value: 'Telecommunications' },
        { label: 'Transpotation', value: 'Transpotation' },
        { label: 'Utlities', value: 'Utlities' },
    ];
    @track stateValue = 'Choose...';
    @track stateOptions = [
        { label: 'Choose...', value: 'Choose...' },
        { label: 'Alabama', value: 'Alabama' },
        { label: 'Alaska', value: 'Alaska' },
        { label: 'Arizona', value: 'Arizona' },
        { label: 'Arkansas', value: 'Arkansas' },
        { label: 'California', value: 'California' },
        { label: 'Colorado', value: 'Colorado' },
        { label: 'Connecticut', value: 'Connecticut' },
        { label: 'Delaware', value: 'Delaware' },
        { label: 'Florida', value: 'Florida' },
        { label: 'Georgia', value: 'Georgia' },
        { label: 'Hawaii', value: 'Hawaii' },
        { label: 'Idaho', value: 'Idaho' },
        { label: 'Illinois', value: 'Illinois' },
        { label: 'Indiana', value: 'Indiana' },
        { label: 'Iowa', value: 'Iowa' },
        { label: 'Kansas', value: 'Kansas' },
        { label: 'Kentucky', value: 'Kentucky' },
        { label: 'Louisiana', value: 'Louisiana' },
        { label: 'Maine', value: 'Maine' },
        { label: 'Maryland', value: 'Maryland' },
        { label: 'Massachusetts', value: 'Massachusetts' },
        { label: 'Michigan', value: 'Michigan' },
        { label: 'Minnesota', value: 'Minnesota' },
        { label: 'Mississippi', value: 'Mississippi' },
        { label: 'Missouri', value: 'Missouri' },
        { label: 'Montana', value: 'Montana' },
        { label: 'Nebraska', value: 'Nebraska' },
        { label: 'Nevada', value: 'Nevada' },
        { label: 'New Hampshire', value: 'New Hampshire' },
        { label: 'New Jersey', value: 'New Jersey' },
        { label: 'New Mexico', value: 'New Mexico' },
        { label: 'New York', value: 'New York' },
        { label: 'North Carolina', value: 'North Carolina' },
        { label: 'North Dakota', value: 'North Dakota' },
        { label: 'Ohio', value: 'Ohio' },
        { label: 'Oklahoma', value: 'Oklahoma' },
        { label: 'Oregon', value: 'Oregon' },
        { label: 'Pennsylvania', value: 'Pennsylvania' },
        { label: 'Rhode Island', value: 'Rhode Island' },
        { label: 'South Carolina', value: 'South Carolina' },
        { label: 'South Dakota', value: 'South Dakota' },
        { label: 'Tennessee', value: 'Tennessee' },
        { label: 'Texas', value: 'Texas' },
        { label: 'Utah', value: 'Utah' },
        { label: 'Vermont', value: 'Vermont' },
        { label: 'Virginia', value: 'Virginia' },
        { label: 'Washington', value: 'Washington' },
        { label: 'West Virginia', value: 'West Virginia' },
        { label: 'Wisconsin', value: 'Wisconsin' },
        { label: 'Wyoming', value: 'Wyoming' },
    ];
    @track dataArray = [];
    @track sameLineDataArray = [];

    connectedCallback() {
        console.log("Test");
        getMetadataOfStep1({
        })
            .then(result => {
                this.getData(result);
            })
            .catch(error => {
                console.log("Error while posting sales1 " + error);
            });
    }

    // gets data from custom meta data that helps to show form structure
    getData(result) {
        let data = result;
        console.log("DataArray1" + JSON.stringify(data));
        this.dataArray = [];
        let tempArr = [];
        data.forEach(element => {
            let _element = Object.assign({}, element);
            if (element.Type__c == "combo box") {
                _element.isInput = false;
            } else {
                _element.isInput = true;
            }
            if (element.MasterLabel == "State") {
                _element.options = this.stateOptions;
                _element.value = this.stateValue;
            }
            if (element.MasterLabel == "Industry") {
                _element.options = this.industryOptions;
                _element.value = this.industryValue;
            }
            element = { ..._element };
            let temp = false;
            tempArr.push(element.Sequence__c);
            let count = 0;
            tempArr.forEach(element1 => {
                if (element1 == element.Sequence__c) {
                    count++;
                    if (count > 1) {
                        temp = true;
                        //index--;
                    }
                }
            });
            if (temp == false) {
                this.dataArray.push({ "key": element.Sequence__c, "value": [], "row": element.Single_Row__c });
            }
            this.dataArray[parseInt(element.Sequence__c) - 1].value.push(element);

        });
        console.log("DataArray3" + JSON.stringify(this.dataArray));

    }

    handleIndustryChange(event) {
        this.value = event.detail.value;
    }
    handleCompanyNameChange(event) {

    }
    handleFerderalTaxIdChange(event) {

    }
    handleBillingAddressChange(event) {

    }
    handleCityChange(event) {

    }
    handleStateChange(event) {

    }
    handleZipCodeChange(event) {

    }
    handleCountryChange(event) {

    }


}