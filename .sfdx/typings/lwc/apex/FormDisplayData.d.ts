declare module "@salesforce/apex/FormDisplayData.getMetadata" {
  export default function getMetadata(): Promise<any>;
}
