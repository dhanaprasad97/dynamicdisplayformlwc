declare module "@salesforce/apex/ImageUpload.uploadFilePhoto" {
  export default function uploadFilePhoto(param: {attachmentId: any, cardholderId: any, name: any}): Promise<any>;
}
